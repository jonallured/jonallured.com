---
title: Edit Files in TextMate from a Remote Server
---

TextMate 2 now includes a [Ruby script](http://blog.macromates.com/2011/mate-and-rmate/) that will allow one to edit files on a remote box locally in TextMate. The script needs to be installed on that server and then you need to do some configuration locally. I don't know, seems like a lot of bullshit to go through.
